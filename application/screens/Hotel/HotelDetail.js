import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, ScrollView, Dimensions, TouchableHighlight} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import Icon from 'react-native-vector-icons/FontAwesome';
import StarRating from "../../components/StarRating";
import MealPlanPicker from "../../components/MealPlanPicker";

export default class HotelDetail extends Component{

    handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
    };

    markers = [
        {
            latitude: 37.78825,
            longitude: -122.4324,
            title: 'Foo Place',
            subtitle: '1234 Foo Drive'
        }
    ];

    state = {
        mapRegion: {
            latitude: 25.80401450217276,
            longitude: -80.12554256655233,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
        },

    };

    render(){
        return(

        <ScrollView>
                {/* Header Card */}
            <View style={styles.container}>
                <Image
                    style={styles.stretch}
                    source={{uri: this.props.navigation.state.params.item.avatar_url}}
                />

                <View style={styles.flatview}>
                    <Text style={styles.headerHotelName}>
                        {this.props.navigation.state.params.item.name}
                    </Text>

                    <View style={styles.starContainer}>
                        <StarRating style={styles.stars} votes={this.props.navigation.state.params.item.stars.value} color="yellow"/>
                    </View>

                    <View styles={styles.pointContainer}>
                        <Icon key={1} style={{opacity: 0.54}}  size={20} name="map-marker" color={'#000000'}>
                            <Text style={styles.txtDescrip}>  6261 Collins, Miami Beach, Florida, EEUU</Text>
                        </Icon>

                    </View>



                    <MapView
                        style={{ alignSelf: 'stretch', height: 200, marginTop: 10 }}
                        region={this.state.mapRegion}
                        annotations={this.markers}
                        onRegionChange={this.handleMapRegionChange}
                    >

                        <Marker
                            coordinate={{latitude: 25.80401450217276,
                                longitude: -80.12554256655233}}
                            title={this.props.navigation.state.params.name}
                            description={"Hotel 5 Estrellas"}
                        />

                    </MapView>

                </View>

                {/* Opinion Card */}
                <View style={styles.flatview}>
                    <View style={styles.miniContainer}>
                        <Text style={styles.titleContainer}>Opiniones</Text>
                        <Text style={styles.opinionesValue}> 390 opiniones</Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={{paddingBottom: 4}}>
                        <Text style={styles.calificacion}>Clasificado en el puesto no. 1.267 de 1.807</Text>
                        <Text style={styles.calificacionValue}>hoteles en Miami</Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.miniContainerOpinion}>
                        <Image
                            style={{height: 50, width: 50}}
                            source={require('../../../assets/images/lady.jpg')}
                        />
                        <View style={styles.miniContainerColumn}>
                            <Text style={styles.textOpinionTitle}>"Un Gran Hotel"</Text>
                            <Text style={styles.textOpinionFooter}>Ubicacion del hotel muy buena...</Text>
                        </View>
                    </View>
                </View>

                {/* Service Card */}
                <View style={styles.flatviewAcc}>
                    <View style={{flexDirection: 'row'}}>
                    <Icon key={1} name="wifi" size={50} color={'#FFFFFF'} style={{paddingLeft:30}}/>
                    <Icon key={2} name="car" size={50} color={'#FFFFFF'} style={{paddingLeft:50}}/>
                    <Icon key={3} name="free-code-camp" size={50} color={'#FFFFFF'} style={{paddingLeft:50}}/>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                    <Text style={{paddingLeft:40, color: '#FFFFFF', fontSize: 10}}>Internet</Text>
                    <Text style={{paddingLeft:53,  color: '#FFFFFF', fontSize: 10}}>Estacionamiento</Text>
                    <Text style={{paddingLeft:47, width: 100, textAlign: 'center',  color: '#FFFFFF', fontSize: 10}}>Permite Fogatas</Text>
                    </View>
                </View>

                {/* Room Card */}
                <View style={styles.flatview}>
                    <View style={styles.miniContainer}>
                        <Text style={styles.titleContainer}>Habitaciones</Text>
                        <Text style={styles.textRightHab}>
                            <Icon key={1} style={{opacity: 0.54}}  size={14} name="users" color={'#000000'}/> 1
                            <Icon key={2} size={14} style={{opacity: 0.54}} color={'#000000'} name="bed"/> 2
                        </Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={{paddingBottom: 4, paddingTop: 4}}>
                        <Text style={styles.txtDetalleHab}>Habitación Standard</Text>
                        <Text style={styles.txtDetalleHabVal}>2 personas | 1 cama doble</Text>
                        <Text style={styles.txtDetalleHab}>Habitación Standard</Text>
                        <Text style={styles.txtDetalleHabVal}>4 personas | 1 cama doble | 2 camas simples</Text>
                    </View>
                    <View style={styles.divider}/>
                        <MealPlanPicker servicios={this.props.navigation.state.params.item.roomService}/>
                    <View style={styles.divider}/>
                    <View style={styles.footerHeighCenter}>
                        <View style={styles.miniContainer}>
                            <Text style={{textAlign: 'center', lineHeight: 28,}}>Precio por noche por habitación</Text>
                            <Text style={styles.textRightPrice}>ARS <Text style={{fontWeight: 'bold'}}>{this.props.navigation.state.params.item.price} </Text></Text>
                        </View>
                    </View>
                    <View style={styles.divider}/>
                    <View styles={styles.miniContainer}>
                        <Text style={styles.textChange}>CAMBIAR</Text>
                    </View>
                </View>

                {/* Description Card */}
                <View style={styles.flatview}>
                    <View style={styles.miniContainer}>
                        <Text style={styles.titleContainer}>Descripción</Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.miniContainerOpinion}>
                        <Icon key={1} style={{opacity: 0.54, paddingTop: 5}}  size={20} name="clock-o" color={'#000000'}/>
                        <View style={styles.miniContainerTimeColumn}>
                            <Text style={styles.txtDescrip}>Entrada: A partir de las 15:00 hs</Text>
                            <Text style={styles.txtDescrip}>Salida: Hasta las 12:00</Text>
                        </View>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.locationContainer}>
                        <Text style={styles.txtDetalleHab}>Ubicación del establecimineto</Text>
                        <Text style={styles.txtDescrip}>En Miami Beach (Mid Beach), Hilton Cabana</Text>
                        <Text style={styles.txtDescrip}>Miami Beach te permite llegar cómodamente a...</Text>
                    </View>
                </View>

                {/* Service Card */}
                <View style={styles.flatview}>
                    <View style={styles.serviceContainer}>
                        <Text style={styles.opiniones}>Cancelación/Prepago</Text>
                        <Text style={styles.txtDescrip}>Las condiciones de cancelación y de pago por adelantado
                                                            pueden variar segun el tipo de...</Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.serviceContainer}>
                        <Text style={styles.opiniones}>Servicios opcionales de habitación</Text>
                        <Text style={styles.txtDescrip}>Los siguientes cargos y depósitos se pagan directamente
                                                        en el hotel al recibir el servicio...</Text>
                    </View>
                    <View style={styles.divider}/>
                    <View style={styles.serviceContainer}>
                        <Text style={styles.opiniones}>Servicios incluídos en la reserva</Text>
                        <Text style={styles.txtDescrip}>Los siguientes cargos se pagan en el hotel:</Text>
                    </View>
                </View>

            </View>
            </ScrollView>
        )
    }
}

const {height, width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#E6E6E6',
        alignItems: 'center',
        margin: 0,
        padding: 0,
        justifyContent: 'center',
        position: "relative",
    },
    pointContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 5,
        paddingBottom: 5,
    },
    titleContainer: {
        flex: 1,
        fontFamily: 'roboto-regular',
        fontSize: 16,
        color: '#000000',
        opacity: 0.87,
    },
    textRightPrice: {
        alignItems: 'flex-end',
        textAlign: 'right',
        fontSize: 20,
        opacity: 0.87,
        flex: 1,
    },
    textChange: {
        padding: 10,
        alignItems: 'flex-end',
        textAlign: 'right',
        fontSize: 14,
        color: '#DF6800',
        fontFamily: 'roboto-medium',
        flex: 1,
    },
    textRightHab: {
        alignItems: 'flex-end',
        textAlign: 'right',
        flex:1,
        fontSize: 14,
        color: '#335692',
        opacity: 1,
        fontFamily: 'roboto-regular'
    },
    headerHotelName:{
        fontSize: 20,
        fontWeight: 'bold',
        fontFamily: 'roboto-regular'
    },
    footerHeigh: {
        flexDirection: 'column',
        flex: 1,
    },
    locationContainer: {
        flexDirection: 'column',
        flex: 1,
        paddingTop: 5,
        paddingBottom: 5,
    },
    footerHeighCenter: {
        flexDirection: 'column',
        flex: 1,
        alignItems: 'center',
        textAlign: 'center',
        justifyContent: 'center',
    },
    divider: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
    },
    miniContainer:{
        flex: 1,
        flexDirection: 'row',
        marginBottom: 3,
    },
    miniContainerOpinion:{
        flex: 1,
        flexDirection: 'row',
        paddingLeft: 5,
        paddingTop: 5,
        paddingBottom: 5,
    },
    miniContainerColumn:{
        flex: 1,
        flexDirection: 'column',
        paddingLeft: 5,
    },
    miniContainerTimeColumn:{
        flex: 1,
        flexDirection: 'column',
        paddingLeft: 25,
    },
    stretch: {
        width: width,
        height: 300,
        padding:0,
        margin: 0,
        alignItems: 'center',
        justifyContent: 'center',
    },
    flatview: {
        flex: 1,
        justifyContent: 'center',
        padding: 5,
        margin: 10,
        backgroundColor: '#fff',
        borderRadius:3,
        width: width-10,
    },
    flatviewAcc: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        padding: 15,
        margin: 5,
        borderRadius:3,
        backgroundColor: '#263e7e',
        width: width,
    },
    flatview2: {
        flex: 1,
        width: width-10,
        justifyContent: 'center',
        padding: 5,
        borderRadius:3,
        margin: 5,
        backgroundColor: '#fff',
    },
    opinionesValue:{
        fontFamily: 'roboto-regular',
        fontSize: 12,
        color: '#000000',
        opacity: 0.54,
        alignItems: 'flex-end',
        textAlign: 'right',
        flex:1,
        lineHeight: 24,
    },
    calificacion: {
        paddingTop: 5,
        fontFamily: 'roboto-medium',
        fontSize: 14,
        color: '#678D44',
        opacity: 1,
        lineHeight: 16,
    },
    calificacionValue: {
        fontFamily: 'roboto-medium',
        fontSize: 12,
        color: '#000000',
        opacity: 0.54,
    },
    textOpinionTitle: {
        fontFamily: 'roboto-regular',
        fontSize: 14,
        color: '#335692',
        opacity: 1,
        paddingTop: 6,
    },
    textOpinionFooter: {
        width: width,
        fontFamily: 'roboto-regular',
        fontSize: 12,
        lineHeight: 14,
        color: '#000000',
        opacity: 0.54,
    },
    txtDetalleHab: {
        fontFamily: 'roboto-regular',
        fontSize: 16,
        color: '#334592',
    },
    txtDetalleHabVal:{
        fontFamily: 'roboto-regular',
        fontSize: 12,
        color: '#000000',
        opacity: 0.54,
        flex:1,
    },
    txtDescrip:{
        fontFamily: 'roboto-regular',
        fontSize: 14,
        color: '#000000',
        opacity: 0.54,
        lineHeight: 20,
        flex:1,
    },
    serviceContainer:{
      paddingBottom: 4,
      paddingTop: 4
    },
    starContainer:{
      flexDirection: 'row',
        flex: 1,
    },

});