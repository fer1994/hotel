import React, {Component} from 'react';
import {StyleSheet, FlatList, View, Image, Text, TouchableWithoutFeedback, Dimensions, TextInput} from 'react-native';
import StarRating from "../../components/StarRating";
import {createStackNavigator} from "react-navigation";
import HotelDetail from "./HotelDetail";
import NavigationActions from "react-navigation/src/NavigationActions";
import Icon from "react-native-vector-icons/FontAwesome";


export default class Hotel extends Component{

    list = [
        {
            id: 1,
            name: 'HOTEL BURJ AL ARAB',
            avatar_url: 'http://www.mancoramarina.com/images/fondo/home_fondo_3.jpg',
            subtitle: 'Vice President',
            price: 1000,
            stars: {
                value: 1,
                color: 'yellow'
            },
            roomService:[
                {id: 1, icon:"bed", label:"Solo la habitacion"},
                {id: 2, icon:"coffee", label:"Desayuno"},
            ],
        },
        {
            id: 2,
            name: 'Cesar Palace',
            avatar_url: 'https://cdn.civitatis.com/estados-unidos/las-vegas/guia/caesars-palace-grid-m.jpg',
            subtitle: 'Vice Chairman',
            price: 300,
            stars: {
                value: 4,
                color: 'yellow'
            },
            roomService:[
                {id: 1, icon:"bed", label:"Solo la habitacion"},
                {id: 2, icon:"coffee", label:"Desayuno"},
            ],
        },
        {
            id: 3,
            name: 'GALLERY HOTEL ART',
            avatar_url: 'https://img.ev.mu/babylon/me/meh/hotel-pas-cher.jpg',
            subtitle: 'Vice Chairman',
            price: 2000,
            stars: {
                value: 2,
                color: 'yellow'
            },
            roomService:[
                {id: 1, icon:"bed", label:"Solo la habitacion"},
                {id: 2, icon:"coffee", label:"Desayuno"},
            ],
        },
        {
            id: 4,
            name: 'HOTEL DE GLACE',
            avatar_url: 'http://www.springhoteles.com/media/cache/thumb436x365/content/files/1/sections/1_inicio/neutral/cabecera/piscinaVul-1.jpg',
            subtitle: 'Vice Chairman',
            price: 10000,
            stars: {
                value: 4,
                color: 'yellow'
            },
            roomService:[
                {id: 1, icon:"bed", label:"Solo la habitacion"},
                {id: 2, icon:"coffee", label:"Desayuno"},
            ],
        },
        {
            id: 5,
            name: 'PALMS CASINO RESORT',
            avatar_url: 'https://www.mixhotels.com/dms/multiHotel-Mix-Hotels/home/alea-home.jpg',
            subtitle: 'Vice Chairman',
            price: 5000,
            stars: {
                value: 4,
                color: 'yellow'
            },
            roomService: [
                {id: 1, icon:"bed", label:"Solo la habitacion"},
                {id: 2, icon:"coffee", label:"Desayuno"},
            ],
        },
        {
            id: 6,
            name: 'MANDARIN ORIENTAL',
            avatar_url: 'http://finde.latercera.com/wp-content/uploads/2018/11/Astoreca-3-ok-700x450.jpg',
            subtitle: 'Vice Chairman',
            price: 1504,
            stars: {
                value: 4,
                color: 'yellow'
            },
            roomService:[
                {id: 1, icon:"bed", label:"Solo la habitacion"},
                {id: 2, icon:"coffee", label:"Desayuno"},
            ],

        },
    ];

    state = {
        hotels: this.list
    };

    goDetail(items){
        const navigateAction = NavigationActions.navigate({
            routeName: 'HotelDetail',
            params: {'item': items},
        })
        this.props.navigation.dispatch(navigateAction);
    }

    render() {
        return (

            <View>

                <View style={styles.textContainer}>
                    <Icon
                        name='search'
                        color='#717171'
                        style={styles.searchIcon}
                        size={14}
                    />
                    <TextInput
                        style={styles.inputStyle}
                        placeholder="Escribí el hotel que buscas"
                    />
                    <Icon
                        name='microphone'
                        color='#717171'
                        style={styles.searchIcon}
                        size={14}
                    />
                </View>


            <FlatList
            data={this.state.hotels}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) =>
                <View style={styles.container}>
                    <TouchableWithoutFeedback onPress={() => this.goDetail(item)}>
                    <View style={styles.flatview} elevation={5}>
                        <Image
                            style={styles.stretch}
                            source={{uri: item.avatar_url}}
                        />
                        <Text style={styles.name}>{item.name}</Text>
                        <View style={styles.footerContainer}>
                            <View style={styles.containerColumn}>
                                <View style={styles.stars}>
                                 <StarRating votes={item.stars.value} color={item.stars.color}/>
                                </View>
                                <View style={styles.footerContainer}>
                                <Icon style={styles.iconPadding} key={item.roomService[1].id} name={item.roomService[1].icon} size={10} />
                                <Text style={styles.txtDescrip}>{item.roomService[1].label}</Text>
                                </View>
                            </View>
                            <View style={styles.footerHeigh}>
                                <Text style={styles.textRightNight}>Precio por noche</Text>
                                <Text style={styles.textRight1}>ARS
                                    <Text style={styles.textRight}> {item.price}
                                    </Text>
                                </Text>
                            </View>
                        </View>
                    </View>
                    </TouchableWithoutFeedback>
                </View>
            }
            keyExtractor={item => item.name}
            />
            </View>
        );
    }

}

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconPadding:{
      paddingTop: 4,
        paddingRight: 3,
    },
    containerColumn:{
        flexDirection: 'column',
        flex: 1,
    },
    textRight1: {
        alignItems: 'flex-end',
        textAlign: 'right',
        fontSize: 20,
        flex: 1,
    },
    textRight: {
        alignItems: 'flex-end',
        textAlign: 'right',
        fontSize: 20,
        fontWeight: 'bold',
    },
    textRightNight: {
        alignItems: 'flex-end',
        textAlign: 'right',
        color: '#A4A4A4',
    },
    stars:{
        flex: 1,
        flexDirection: 'row',
        paddingTop: 10,
    },
    h2text: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 36,
        fontWeight: 'bold',
    },
    flatview: {
        justifyContent: 'center',
        padding: 5,
        margin: 5,
        backgroundColor: '#fff',
        borderRadius:4,
    },
    footerContainer: {
        flexDirection: 'row',
        flex: 1,
    },
    footerContainer2: {
        flexDirection: 'row',
        textAlign: 'right',
        alignItems: 'flex-end',
        flex: 1,
    },
    footerHeigh: {
        flexDirection: 'column',
        flex: 1,
        textAlign: 'right',
        alignItems: 'flex-end',
    },
    name: {
        fontSize: 14,
        fontWeight: 'bold'
    },
    stretch: {
        height: 200,
        width: width-20,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    txtDescrip:{
        color: '#000000',
        opacity: 0.54,
        flex:1,
    },
    searchIcon: {
        padding: 10,
    },
    textContainer: {
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 5,
        margin: 5,
        marginTop: 35,
        backgroundColor: '#fff',
        borderRadius:4,
    },
    inputStyle: {
        flex: 1,
        backgroundColor: '#fff',
    },

});