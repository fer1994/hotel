import React, {Component} from 'react';
import {FlatList, StyleSheet, Text, View, TouchableHighlight} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';

export default class MealPlanPicker extends Component {



    constructor(props) {
        super(props);

        this.state = {showOptions: false, itemSelec: null};
        this.onPressButton = this.onPressButton.bind(this);
        this.onSelectItem = this.onSelectItem.bind(this);
    }

    render(){

            const { servicios } = this.props;
            var item;

            if(this.state.showOptions) {
                item =this.returnList(servicios)
            }else{
                item = this.returnItem(this.state.itemSelec ? this.state.itemSelec : servicios[0]);
            }

            return <View>{item}</View>

    }

    onPressButton(item){
        this.setState({ showOptions: !this.state.showOptions, itemSelec: item });
    }

    onSelectItem(item) {

        this.returnItem(item)
        this.setState({ showOptions: !this.state.showOptions, itemSelec: item });
    }

    returnItem(item){
        return <View style={styles.container}>

            <Icon style={styles.icons} key={1} name={item.icon} size={35} />
            <Text style={styles.textLabel}>{item.label}</Text>
            <TouchableHighlight onPress={() => this.onPressButton(item)}>
                <Icon key={1} name="angle-down" size={35} color={'#d37129'}/>
            </TouchableHighlight>
        </View>
    }

    returnList(list){
        return  <View>
            <FlatList
                data={list}
                showsVerticalScrollIndicator={false}
                renderItem={({item}) =>
                    <TouchableHighlight onPress={() => this.onSelectItem(item)}>
                    <View style={styles.container}>
                        <Icon style={styles.icons} key={item.id} name={item.icon} size={35} />
                        <Text style={styles.textLabel}>{item.label}</Text>
                        {this.state.itemSelec.id === item.id ? <Icon key={item.id+2} disp name={'check-circle'} size={25} color={'#d37129'}/> : null }
                    </View>
                    </TouchableHighlight>
                }
                keyExtractor={item => item.label}
            />
        </View>
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 5,
        paddingBottom: 5,
    },
    textLabel: {
        fontSize: 20,
        color: '#000000',
        opacity: 0.54,
        lineHeight: 24,
        flex:1,
        fontWeight: 'bold',
        paddingLeft: 5,
        fontFamily: 'roboto-regular',
    },
    icons: {
        opacity: 0.54,
    },
});

