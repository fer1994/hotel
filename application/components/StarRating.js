import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class StarRating extends Component{

    render(){
        return(
            <Icon styles={styles.wrapper}>
                {this.stars}
            </Icon>

        )
    }

    get stars(){
        const { votes } = this.props;
        const starNumber = parseInt(votes);
        const starElement = [];

        for (let i = 0; i < 5; i++) {
            starElement.push(
                <Icon style={styles.star} key={i} name="star" size={15} color={starNumber > i ? '#F4FA58' : '#BDBDBD'} />
            )
        }
        return starElement;
    }

}

const styles = StyleSheet.create({
    wrapper: {
        flexDirection: 'row',
        flex: 1,
    },
    star: {
        marginLeft: 1,
    }
});