import React, {Component} from 'React';

import {StackNavigator} from 'react-navigation';
import Hotel from "../screens/Hotel/Hotel";
import HotelDetail from '../screens/Hotel/HotelDetail'

export default StackNavigator(
    {
        Hotel:{
            screen: Hotel
        },
        HotelDetail: {
            screen: HotelDetail,
            navigationOptions: {
                title: 'Detalle'
            }
        }
    },
    {
        initialRouteName: 'Hotel',
        navigationOptions: {
            headerStyle: {
                position: 'absolute',
                backgroundColor: 'transparent',
                zIndex: 100,
                top: 0,
                left: 0,
                right: 0,
                border: 0,
            },
            headerTitleStyle: {
                color: '#fff',
                fontSize: 20,
                alignItems: 'flex-start',
                textAlign: 'left',
            },
            headerTintColor: '#fff',
        }
    }
);