import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStackNavigator} from "react-navigation";
import GuestNavigation from './application/navigations/navigationHotel'
import { Font } from 'expo';


export default class App extends React.Component {
  render() {
    return (
        <GuestNavigation/>
    );
  }

  componentDidMount() {
        Font.loadAsync({
            'roboto-regular': require('./assets/fonts/Roboto-Regular.ttf'),
            'roboto-medium': require('./assets/fonts/Roboto-Medium.ttf'),
        });
    }

}

